#!/bin/bash

# Most low-level git command won't work without a HEAD file,
# so we create a dummy one that points to nothing
rm -Rf .git
mkdir -p .git/refs/heads
echo "ref: refs/heads/master" > .git/HEAD


# Let's store some content
content="test content
"

# Calculate content lenght in bytes, not in charaters
content_length=${#content}

# Format the blob object
blob="blob ${content_length}\x00${content}"

# Calculate the hash
hash="$(echo -ne "$blob" | sha1sum | cut -d' ' -f1)"

# Deduce storage path
path=".git/objects/${hash:0:2}/${hash:2}"

# Compress the blob object and store it
mkdir -p $(dirname $path)
echo -ne "$blob" | zlib-flate -compress > $path

# Verify
echo "Created object $hash"
echo " - type: $(git cat-file -t $hash)"
echo " - size: $(git cat-file -s $hash)"
echo " - content: $(git cat-file -p $hash)"
