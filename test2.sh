#!/bin/bash

set -e 

rm -Rf .git
mkdir -p .git/refs/heads
echo "ref: refs/heads/master" > .git/HEAD

function hash() { echo -ne "$1" | sha1sum | cut -d' ' -f1; }

function create_object() {
    object_type=$1
    content_path=$2
    content_length=$(stat --printf="%s" $content_path)
    content=$(od -An -tx1 -v $content_path  | sed -e 's/ /\\x/g' |tr -d '\n')
    object="$object_type ${content_length}\x00${content}"
    object_hash=$(hash "$object")
    object_path=".git/objects/${object_hash:0:2}/${object_hash:2}"
    mkdir -p $(dirname $object_path)
    echo -ne "$object" | zlib-flate -compress > $object_path
    echo -n "$object_hash"
}

function tree_item()
{
    mode=$1
    name=$2
    hexa_hash=$3
    binary_hash=$(echo $hexa_hash | sed -e 's/../\\x&/g')
    echo -n "$mode $name\x00$binary_hash"
}

# First file
echo -ne 'test content\n' > file1
blob1_hash=$(create_object blob file1)
# Second file
echo -ne 'test more content\ntest more content\n' > file2
blob2_hash=$(create_object blob file2)
# Magic null tree
tree0_hash=$(create_object tree /dev/null) 
# Create the tree object: file1, file2 and an empty dir1
file1_entry=$(tree_item 100644 file1 $blob1_hash)
file2_entry=$(tree_item 100644 file2 $blob2_hash)
dir1_entry=$(tree_item 40000 dir1 $tree0_hash)
echo -ne "$file1_entry$file2_entry$dir1_entry" > fs1
tree1_hash=$(create_object tree fs1)
# cleanup
rm -f file1 file2 fs1
echo "Created a Tree object ${tree1_hash}"
git cat-file -p $tree1_hash
exit 0
